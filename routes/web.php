<?php

use App\Http\Controllers\Article\ArticleController;
use Illuminate\Support\Facades\Route;


Route::get("/article", [ArticleController::class, "browse"])->name("article.browse");
Route::post("/article", [ArticleController::class, "add"])->name("article.add");
Route::get("/article/{id}", [ArticleController::class, "read"])->name("article.read");
Route::get("/article/{id}/update", [ArticleController::class, "update_form"])->name("article.edit.form");
Route::put("/article/{id}", [ArticleController::class, "edit"])->name("article.edit");
Route::delete("/article/{id}", [ArticleController::class, "destroy"])->name("article.destroy");