<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Blog</title>
    @vite("resources/css/app.css")
  </head>

  <body>
    <x-navbar />

    @yield("content")

    @vite("resources/js/app.js")
  </body>

</html>