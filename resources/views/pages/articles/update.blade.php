@extends("layouts.master")

@section("content")
<form action="{{ route("article.edit", $article->id) }}" method="POST" class="card">
  @method("PUT")
  <div class="card-header text-center bg-primary text-white">Status Update</div>

  <div class="card-body">
    <div class="form-group mb-2">
      <input type="text" name="title" value="{{ $article->title }}" class="form-control">
    </div>

    @csrf

    <div class="form-group">
      <textarea name="body" id="body" class="form-control">{{ $article->body }}</textarea>
    </div>
  </div>

  <div class="card-footer d-grid">
    <button type="submit" class="btn btn-warning">Save</button>
  </div>
</form>
@endsection