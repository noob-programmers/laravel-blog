@extends("layouts.master")

@section("content")
  <section class="container-fluid mt-2">
    <div class="row">
      <div class="col-lg-3">
        <x-status-update-field />
      </div>

      <div class="col-lg-9">
        @foreach ($articles as $article)
          <x-article-card :article="$article" />
        @endforeach
      </div>
    </div>
  </section>
@endsection