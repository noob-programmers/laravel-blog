@extends("layouts.master")

@section("content")
<article class="card mb-2">
  <div class="card-header text-white bg-primary text-center">
    <strong>{{ $article->title }}</strong>
  </div>

  <div class="card-body">{{ $article->body }}</div>
</article>
@endsection