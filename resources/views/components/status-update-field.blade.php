<form action="{{ route("article.add") }}" method="POST" class="card">
  <div class="card-header bg-primary text-center text-white">Status Update</div>

  <div class="card-body">
    <div class="form-group mb-2">
      <input type="text" name="title" placeholder="What do you wanna talk about?" class="form-control">
    </div>

    @csrf

    <div class="form-group">
      <textarea name="body" id="body" placeholder="What's on your mind?" cols="30" rows="10" class="form-control"></textarea>
    </div>
  </div>

  <div class="card-footer d-grid">
    <button type="submit" class="btn btn-primary">Post</button>
  </div>
</form>