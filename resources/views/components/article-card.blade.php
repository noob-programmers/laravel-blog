<article class="card mb-2">
  <div class="card-header text-white bg-primary text-center">
    <strong>{{ $article->title }}</strong>
  </div>

  <div class="card-body">{{ $article->body }}</div>

  <div class="card-footer">
    <div class="btn btn-group">
      <a class="btn btn-primary" href="{{ route("article.read", $article->id) }}">See More</a>
      <a class="btn btn-warning" href="{{ route("article.edit.form", $article->id) }}">Update Article</a>

      <form action="{{ route("article.destroy", $article->id) }}" method="POST">
        @method("DELETE")

        @csrf
        <button class="btn btn-danger">Delete Article</button>
      </form>
    </div>
  </div>
</article>