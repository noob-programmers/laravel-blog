<?php

namespace App\Http\Controllers\Article;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Blog;
use Illuminate\Http\Request;

class ArticleController extends Controller {
  public function browse() {
    $articles = Blog::all();

    return view("pages.articles.dashboard")->with([
      "articles" => $articles
    ]);
  }

  public function add(ArticleRequest $request) {
    $title = $request->input("title");
    $body = $request->input("body");

    $article = new Blog();

    $article->title = $title;
    $article->body = $body;

    $article->save();

    return redirect()->route("article.browse");
  }

  public function read($id) {
    $blog = Blog::find($id);

    return view("pages.articles.article")->with("article", $blog);
  }

  public function update_form($id) {
    $blog = Blog::find($id);

    return view("pages.articles.update")->with("article", $blog);
  }

  public function edit($id, ArticleRequest $request) {
    $title = $request->input("title");
    $body = $request->input("body");

    $article = Blog::find($id);

    $article->title = $title;
    $article->body = $body;

    $article->save();

    return redirect()->route("article.browse");
  }

  public function destroy($id) {
    Blog::where("id", $id)->delete();

    return redirect()->route("article.browse");
  }
}